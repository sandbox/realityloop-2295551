<?php
/**
 * @file
 * Context reaction plugin for Contextual permissions.
 */

/**
 * Apply image styles to favicon as context reactions.
 */
class context_reaction_permission extends context_reaction {
  /**
   * Options form.
   */
  function options_form($context) {
    $form = array();

    $form['permissions'] = array(
      '#type' => 'container',
      '#prefix' => '<div id="context-permissions-ajax-wrapper">',
      '#suffix' => '</div>',
      '#theme' => 'context_permission_form',
    );

    $form['permissions']['permission'] = array();
    if (!empty($context->reactions['permission'])) {
      // Retrieve role names for columns.
      $role_names = user_roles();

      $form['permissions']['remove'] = array();
      foreach ($context->reactions['permission'] as $permission => $value) {
        $form['permissions']['permission'][$permission] = array(
          '#title' => $permission,
          '#type' => 'checkboxes',
          '#options' => $role_names,
          '#default_value' => $value,
        );

        $form['permissions']['remove'][$permission] = array(
          '#type' => 'button',
          '#value' => t('Remove'),
          '#name' => "permission-{$permission}",
          '#ajax' => array(
            'callback' => 'context_permission_options_form_ajax',
            'wrapper' => 'context-permissions-ajax-wrapper',
          ),
          '#limit_validation_errors' => TRUE,
        );
      }
    }

    // Add a permission.
    $form['permissions']['add'] = array(
      '#type' => 'container',
    );
    $form['permissions']['add']['name'] = array(
      '#type' => 'select',
      '#title' => t('Permission'),
      '#options' => $this->permissions_get(array_keys($form['permissions']['permission'])),
    );
    $form['permissions']['add']['button'] = array(
      '#type' => 'button',
      '#value' => t('Add permission'),
      '#ajax' => array(
        'callback' => 'context_permission_options_form_ajax',
        'wrapper' => 'context-permissions-ajax-wrapper',
      ),
      '#limit_validation_errors' => TRUE,
    );

    return $form;
  }

  /**
   * Get a list of permissions defined by hook_permission().
   *
   * @param $exclude
   *   An optional array of permissions to exclude from the list of returned
   *   permissions.
   *
   * @return
   *   A nested array of permissions, grouped by their module.
   */
  function permissions_get($exclude = array()) {
    $permissions = array();

    $module_info = system_get_info('module');
    foreach (module_implements('permission') as $module) {
      foreach (module_invoke($module, 'permission') as $name => $permission) {
        if (!in_array($name, $exclude)) {
          if (!isset($permissions[$module_info[$module]['name']])) {
            $permissions[$module_info[$module]['name']] = array();
          }
          $permissions[$module_info[$module]['name']][$name] = $permission['title'];
        }
      }
    }
    ksort($permissions);
    foreach ($permissions as &$group) {
      if (is_array($group)) {
        ksort($group);
      }
    }
    return $permissions;
  }

  /**
   * Options form submit handler.
   */
  function options_form_submit($values) {
    $permissions = array();

    if (!empty($values['permissions']['permission'])) {
      foreach ($values['permissions']['permission'] as $permission => $value) {
        $permissions[$permission] = array_filter($value);
      }
    }

    // AJAX handler for Add and Remove buttons.
    if (strstr(request_uri(), 'system/ajax')) {
      $form_state = array('submitted' => FALSE);
      $form_build_id = $_POST['form_build_id'];
      $form = form_get_cache($form_build_id, $form_state);

      $form_state['input'] = $_POST;
      $form_state['values'] = array();
      $form = form_builder($form['#form_id'], $form, $form_state);

      switch ($form_state['triggering_element']['#parents']) {
        case array('reactions', 'plugins', 'permission', 'permissions', 'add', 'button'):
          $permission = $values['permissions']['add']['name'];

          $rids = db_select('role_permission', 'p')
            ->fields('p', array('rid'))
            ->condition('permission', $permission)
            ->execute()
            ->fetchCol(0);
          $permissions[$permission] = $rids;
          break;

        case array('reactions', 'plugins', 'permission', 'permissions', 'remove', drupal_substr($form_state['triggering_element']['#name'], 11)):
          $name = drupal_substr($form_state['triggering_element']['#name'], 11);
          unset($permissions[$name]);
          break;
      }
    }

    ksort($permissions);
    return $permissions;
  }

  /**
   * Execute.
   */
  function execute() {
    foreach ($this->get_contexts() as $context) {
      if (isset($context->reactions['permission'])) {
        $permissions = user_role_permissions(user_roles());

        $changes = array();
        foreach (element_children($context->reactions['permission']) as $permission) {
          $rids = $context->reactions['permission'][$permission];
          foreach (user_roles() as $rid => $name) {
            if (isset($rids[$rid]) && !isset($permissions[$rid][$permission])) {
              $changes[$rid][$permission] = TRUE;
            }
            elseif (!isset($rids[$rid]) && isset($permissions[$rid][$permission])) {
              $changes[$rid][$permission] = FALSE;
            }
          }
        }
        if (!empty($changes)) {
          foreach ($changes as $rid => $perms) {
            user_role_change_permissions($rid, $perms);
          }
          // @TODO - Force redirect to ensure permissions are changed?
        }
      }
    }
  }
}

/**
 * AJAX callback for Context reaction options form.
 */
function context_permission_options_form_ajax($form, $form_state) {
  return $form['reactions']['plugins']['permission']['permissions'];
}
