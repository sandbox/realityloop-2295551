The Contextual permissions module allows for Permissions to be temporarily
modified using the Context module, for example, you could prevent the ability
to create Nodes during certain times of the day.

Contextual permissions was written by Stuart Clark (deciphered) and is
maintained by Realityloop pty ltd.
- http://realityloop.com
- http://twitter.com/Realityloop



Required modules
--------------------------------------------------------------------------------

* Context  - http://drupal.org/project/context
